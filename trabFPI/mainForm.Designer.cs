﻿namespace trabFPI
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.arquivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.salvarStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.fecharToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.espelharImagemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tonsDeCinzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quantizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.desfazerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descartarMudançasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBoxOriginal = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBoxModificada = new System.Windows.Forms.PictureBox();
            this.groupBoxZoom = new System.Windows.Forms.GroupBox();
            this.numericUpDownZoomOutY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownZoomOutX = new System.Windows.Forms.NumericUpDown();
            this.buttonZoomOut = new System.Windows.Forms.Button();
            this.buttonZoomIn = new System.Windows.Forms.Button();
            this.groupBoxKernel = new System.Windows.Forms.GroupBox();
            this.buttonConvolution = new System.Windows.Forms.Button();
            this.radioButtonNeutro = new System.Windows.Forms.RadioButton();
            this.radioButtonSobelHy = new System.Windows.Forms.RadioButton();
            this.radioButtonSobelHx = new System.Windows.Forms.RadioButton();
            this.radioButtonPrewittHy = new System.Windows.Forms.RadioButton();
            this.radioButtonPrewittHx = new System.Windows.Forms.RadioButton();
            this.radioButtonPassaAltas = new System.Windows.Forms.RadioButton();
            this.radioButtonLaplace = new System.Windows.Forms.RadioButton();
            this.radioButtonGausse = new System.Windows.Forms.RadioButton();
            this.kernel33 = new System.Windows.Forms.NumericUpDown();
            this.kernel23 = new System.Windows.Forms.NumericUpDown();
            this.kernel13 = new System.Windows.Forms.NumericUpDown();
            this.kernel32 = new System.Windows.Forms.NumericUpDown();
            this.kernel22 = new System.Windows.Forms.NumericUpDown();
            this.kernel12 = new System.Windows.Forms.NumericUpDown();
            this.kernel31 = new System.Windows.Forms.NumericUpDown();
            this.kernel21 = new System.Windows.Forms.NumericUpDown();
            this.kernel11 = new System.Windows.Forms.NumericUpDown();
            this.groupBoxRotacionar = new System.Windows.Forms.GroupBox();
            this.buttonRotHor = new System.Windows.Forms.Button();
            this.buttonRotAntiHor = new System.Windows.Forms.Button();
            this.labelEscalaHist = new System.Windows.Forms.Label();
            this.numericUpDownScaleHist = new System.Windows.Forms.NumericUpDown();
            this.boxHistogram = new System.Windows.Forms.GroupBox();
            this.glControl = new OpenTK.GLControl();
            this.buttonNegativo = new System.Windows.Forms.Button();
            this.numericInContraste = new System.Windows.Forms.NumericUpDown();
            this.numericInBrilho = new System.Windows.Forms.NumericUpDown();
            this.buttonContraste = new System.Windows.Forms.Button();
            this.buttonBrilho = new System.Windows.Forms.Button();
            this.buttonEqualizar = new System.Windows.Forms.Button();
            this.labelTons = new System.Windows.Forms.Label();
            this.buttonQuantizar = new System.Windows.Forms.Button();
            this.numericUpDownShades = new System.Windows.Forms.NumericUpDown();
            this.groupBoxEspelhar = new System.Windows.Forms.GroupBox();
            this.buttonEspelhaX = new System.Windows.Forms.Button();
            this.buttonEspelhaY = new System.Windows.Forms.Button();
            this.buttonEscalaCinza = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModificada)).BeginInit();
            this.groupBoxZoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomOutY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomOutX)).BeginInit();
            this.groupBoxKernel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kernel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel11)).BeginInit();
            this.groupBoxRotacionar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleHist)).BeginInit();
            this.boxHistogram.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericInContraste)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericInBrilho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShades)).BeginInit();
            this.groupBoxEspelhar.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arquivoToolStripMenuItem,
            this.imagemToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1394, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // arquivoToolStripMenuItem
            // 
            this.arquivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.salvarToolStripMenuItem,
            this.salvarStripMenuItem,
            this.salvarComoToolStripMenuItem,
            this.toolStripMenuItem1,
            this.fecharToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.arquivoToolStripMenuItem.Name = "arquivoToolStripMenuItem";
            this.arquivoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.arquivoToolStripMenuItem.Text = "&Arquivo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.abrirToolStripMenuItem.Text = "&Abrir imagem";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // salvarToolStripMenuItem
            // 
            this.salvarToolStripMenuItem.Name = "salvarToolStripMenuItem";
            this.salvarToolStripMenuItem.Size = new System.Drawing.Size(162, 6);
            // 
            // salvarStripMenuItem
            // 
            this.salvarStripMenuItem.Name = "salvarStripMenuItem";
            this.salvarStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.salvarStripMenuItem.Text = "&Salvar";
            this.salvarStripMenuItem.Click += new System.EventHandler(this.salvarStripMenuItem_Click);
            // 
            // salvarComoToolStripMenuItem
            // 
            this.salvarComoToolStripMenuItem.Name = "salvarComoToolStripMenuItem";
            this.salvarComoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.salvarComoToolStripMenuItem.Text = "Salvar &como";
            this.salvarComoToolStripMenuItem.Click += new System.EventHandler(this.salvarComoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(162, 6);
            // 
            // fecharToolStripMenuItem
            // 
            this.fecharToolStripMenuItem.Name = "fecharToolStripMenuItem";
            this.fecharToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.fecharToolStripMenuItem.Text = "&Fechar arquivo";
            this.fecharToolStripMenuItem.Click += new System.EventHandler(this.fecharToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.sairToolStripMenuItem.Text = "Sai&r do programa";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // imagemToolStripMenuItem
            // 
            this.imagemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.espelharImagemToolStripMenuItem,
            this.toolStripMenuItem2,
            this.tonsDeCinzaToolStripMenuItem,
            this.quantizarToolStripMenuItem,
            this.toolStripMenuItem3,
            this.desfazerToolStripMenuItem,
            this.descartarMudançasToolStripMenuItem});
            this.imagemToolStripMenuItem.Name = "imagemToolStripMenuItem";
            this.imagemToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.imagemToolStripMenuItem.Text = "&Ferramentas";
            // 
            // espelharImagemToolStripMenuItem
            // 
            this.espelharImagemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontalToolStripMenuItem,
            this.verticalToolStripMenuItem});
            this.espelharImagemToolStripMenuItem.Name = "espelharImagemToolStripMenuItem";
            this.espelharImagemToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.espelharImagemToolStripMenuItem.Text = "Espelhar imagem";
            // 
            // horizontalToolStripMenuItem
            // 
            this.horizontalToolStripMenuItem.Name = "horizontalToolStripMenuItem";
            this.horizontalToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.horizontalToolStripMenuItem.Text = "Horizontal";
            this.horizontalToolStripMenuItem.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // verticalToolStripMenuItem
            // 
            this.verticalToolStripMenuItem.Name = "verticalToolStripMenuItem";
            this.verticalToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.verticalToolStripMenuItem.Text = "Vertical";
            this.verticalToolStripMenuItem.Click += new System.EventHandler(this.verticalToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(171, 6);
            // 
            // tonsDeCinzaToolStripMenuItem
            // 
            this.tonsDeCinzaToolStripMenuItem.Name = "tonsDeCinzaToolStripMenuItem";
            this.tonsDeCinzaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.tonsDeCinzaToolStripMenuItem.Text = "Tons de cinza";
            this.tonsDeCinzaToolStripMenuItem.Click += new System.EventHandler(this.tonsDeCinzaToolStripMenuItem_Click);
            // 
            // quantizarToolStripMenuItem
            // 
            this.quantizarToolStripMenuItem.Name = "quantizarToolStripMenuItem";
            this.quantizarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.quantizarToolStripMenuItem.Text = "Quantizar";
            this.quantizarToolStripMenuItem.Click += new System.EventHandler(this.quantizarToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(171, 6);
            // 
            // desfazerToolStripMenuItem
            // 
            this.desfazerToolStripMenuItem.Name = "desfazerToolStripMenuItem";
            this.desfazerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.desfazerToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.desfazerToolStripMenuItem.Text = "Desfa&zer";
            this.desfazerToolStripMenuItem.Click += new System.EventHandler(this.desfazerToolStripMenuItem_Click);
            // 
            // descartarMudançasToolStripMenuItem
            // 
            this.descartarMudançasToolStripMenuItem.Name = "descartarMudançasToolStripMenuItem";
            this.descartarMudançasToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Q)));
            this.descartarMudançasToolStripMenuItem.ShowShortcutKeys = false;
            this.descartarMudançasToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.descartarMudançasToolStripMenuItem.Text = "Descartar mudanças";
            this.descartarMudançasToolStripMenuItem.Click += new System.EventHandler(this.descartarMudançasToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "JPEG image files|*.jpg";
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Abrir imagem";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "JPEG image files|*.jpg";
            this.saveFileDialog.RestoreDirectory = true;
            this.saveFileDialog.Title = "Salvar imagem";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxZoom);
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxKernel);
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxRotacionar);
            this.splitContainer1.Panel2.Controls.Add(this.labelEscalaHist);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownScaleHist);
            this.splitContainer1.Panel2.Controls.Add(this.boxHistogram);
            this.splitContainer1.Panel2.Controls.Add(this.buttonNegativo);
            this.splitContainer1.Panel2.Controls.Add(this.numericInContraste);
            this.splitContainer1.Panel2.Controls.Add(this.numericInBrilho);
            this.splitContainer1.Panel2.Controls.Add(this.buttonContraste);
            this.splitContainer1.Panel2.Controls.Add(this.buttonBrilho);
            this.splitContainer1.Panel2.Controls.Add(this.buttonEqualizar);
            this.splitContainer1.Panel2.Controls.Add(this.labelTons);
            this.splitContainer1.Panel2.Controls.Add(this.buttonQuantizar);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownShades);
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxEspelhar);
            this.splitContainer1.Panel2.Controls.Add(this.buttonEscalaCinza);
            this.splitContainer1.Size = new System.Drawing.Size(1394, 704);
            this.splitContainer1.SplitterDistance = 1009;
            this.splitContainer1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1007, 702);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.pictureBoxOriginal);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(999, 676);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Imagem original";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBoxOriginal
            // 
            this.pictureBoxOriginal.Location = new System.Drawing.Point(7, 6);
            this.pictureBoxOriginal.Name = "pictureBoxOriginal";
            this.pictureBoxOriginal.Size = new System.Drawing.Size(147, 43);
            this.pictureBoxOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxOriginal.TabIndex = 0;
            this.pictureBoxOriginal.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.pictureBoxModificada);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(999, 676);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Imagem modificada";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBoxModificada
            // 
            this.pictureBoxModificada.Location = new System.Drawing.Point(7, 6);
            this.pictureBoxModificada.Name = "pictureBoxModificada";
            this.pictureBoxModificada.Size = new System.Drawing.Size(111, 47);
            this.pictureBoxModificada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxModificada.TabIndex = 1;
            this.pictureBoxModificada.TabStop = false;
            // 
            // groupBoxZoom
            // 
            this.groupBoxZoom.Controls.Add(this.numericUpDownZoomOutY);
            this.groupBoxZoom.Controls.Add(this.numericUpDownZoomOutX);
            this.groupBoxZoom.Controls.Add(this.buttonZoomOut);
            this.groupBoxZoom.Controls.Add(this.buttonZoomIn);
            this.groupBoxZoom.Location = new System.Drawing.Point(10, 77);
            this.groupBoxZoom.Name = "groupBoxZoom";
            this.groupBoxZoom.Size = new System.Drawing.Size(110, 118);
            this.groupBoxZoom.TabIndex = 21;
            this.groupBoxZoom.TabStop = false;
            this.groupBoxZoom.Text = "Zoom";
            // 
            // numericUpDownZoomOutY
            // 
            this.numericUpDownZoomOutY.Location = new System.Drawing.Point(60, 50);
            this.numericUpDownZoomOutY.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownZoomOutY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownZoomOutY.Name = "numericUpDownZoomOutY";
            this.numericUpDownZoomOutY.Size = new System.Drawing.Size(40, 20);
            this.numericUpDownZoomOutY.TabIndex = 14;
            this.numericUpDownZoomOutY.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numericUpDownZoomOutX
            // 
            this.numericUpDownZoomOutX.Location = new System.Drawing.Point(14, 50);
            this.numericUpDownZoomOutX.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownZoomOutX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownZoomOutX.Name = "numericUpDownZoomOutX";
            this.numericUpDownZoomOutX.Size = new System.Drawing.Size(40, 20);
            this.numericUpDownZoomOutX.TabIndex = 13;
            this.numericUpDownZoomOutX.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // buttonZoomOut
            // 
            this.buttonZoomOut.Location = new System.Drawing.Point(14, 76);
            this.buttonZoomOut.Name = "buttonZoomOut";
            this.buttonZoomOut.Size = new System.Drawing.Size(85, 25);
            this.buttonZoomOut.TabIndex = 2;
            this.buttonZoomOut.Text = "Out";
            this.buttonZoomOut.UseVisualStyleBackColor = true;
            this.buttonZoomOut.Click += new System.EventHandler(this.buttonZoomOut_Click);
            // 
            // buttonZoomIn
            // 
            this.buttonZoomIn.Location = new System.Drawing.Point(14, 19);
            this.buttonZoomIn.Name = "buttonZoomIn";
            this.buttonZoomIn.Size = new System.Drawing.Size(85, 25);
            this.buttonZoomIn.TabIndex = 1;
            this.buttonZoomIn.Text = "In (2x2)";
            this.buttonZoomIn.UseVisualStyleBackColor = true;
            this.buttonZoomIn.Click += new System.EventHandler(this.buttonZoomIn_Click);
            // 
            // groupBoxKernel
            // 
            this.groupBoxKernel.Controls.Add(this.buttonConvolution);
            this.groupBoxKernel.Controls.Add(this.radioButtonNeutro);
            this.groupBoxKernel.Controls.Add(this.radioButtonSobelHy);
            this.groupBoxKernel.Controls.Add(this.radioButtonSobelHx);
            this.groupBoxKernel.Controls.Add(this.radioButtonPrewittHy);
            this.groupBoxKernel.Controls.Add(this.radioButtonPrewittHx);
            this.groupBoxKernel.Controls.Add(this.radioButtonPassaAltas);
            this.groupBoxKernel.Controls.Add(this.radioButtonLaplace);
            this.groupBoxKernel.Controls.Add(this.radioButtonGausse);
            this.groupBoxKernel.Controls.Add(this.kernel33);
            this.groupBoxKernel.Controls.Add(this.kernel23);
            this.groupBoxKernel.Controls.Add(this.kernel13);
            this.groupBoxKernel.Controls.Add(this.kernel32);
            this.groupBoxKernel.Controls.Add(this.kernel22);
            this.groupBoxKernel.Controls.Add(this.kernel12);
            this.groupBoxKernel.Controls.Add(this.kernel31);
            this.groupBoxKernel.Controls.Add(this.kernel21);
            this.groupBoxKernel.Controls.Add(this.kernel11);
            this.groupBoxKernel.Location = new System.Drawing.Point(20, 224);
            this.groupBoxKernel.Name = "groupBoxKernel";
            this.groupBoxKernel.Size = new System.Drawing.Size(348, 195);
            this.groupBoxKernel.TabIndex = 20;
            this.groupBoxKernel.TabStop = false;
            this.groupBoxKernel.Text = "Kernel";
            // 
            // buttonConvolution
            // 
            this.buttonConvolution.Location = new System.Drawing.Point(243, 155);
            this.buttonConvolution.Name = "buttonConvolution";
            this.buttonConvolution.Size = new System.Drawing.Size(96, 25);
            this.buttonConvolution.TabIndex = 29;
            this.buttonConvolution.Text = "Convolução";
            this.buttonConvolution.UseVisualStyleBackColor = true;
            this.buttonConvolution.Click += new System.EventHandler(this.buttonConvolution_Click);
            // 
            // radioButtonNeutro
            // 
            this.radioButtonNeutro.AutoSize = true;
            this.radioButtonNeutro.Checked = true;
            this.radioButtonNeutro.Location = new System.Drawing.Point(226, 132);
            this.radioButtonNeutro.Name = "radioButtonNeutro";
            this.radioButtonNeutro.Size = new System.Drawing.Size(57, 17);
            this.radioButtonNeutro.TabIndex = 28;
            this.radioButtonNeutro.TabStop = true;
            this.radioButtonNeutro.Text = "Neutro";
            this.radioButtonNeutro.UseVisualStyleBackColor = true;
            this.radioButtonNeutro.CheckedChanged += new System.EventHandler(this.radioButtonNeutro_CheckedChanged);
            // 
            // radioButtonSobelHy
            // 
            this.radioButtonSobelHy.AutoSize = true;
            this.radioButtonSobelHy.Location = new System.Drawing.Point(226, 109);
            this.radioButtonSobelHy.Name = "radioButtonSobelHy";
            this.radioButtonSobelHy.Size = new System.Drawing.Size(68, 17);
            this.radioButtonSobelHy.TabIndex = 27;
            this.radioButtonSobelHy.Text = "Sobel Hy";
            this.radioButtonSobelHy.UseVisualStyleBackColor = true;
            this.radioButtonSobelHy.CheckedChanged += new System.EventHandler(this.radioButtonSobelHy_CheckedChanged);
            // 
            // radioButtonSobelHx
            // 
            this.radioButtonSobelHx.AutoSize = true;
            this.radioButtonSobelHx.Location = new System.Drawing.Point(115, 155);
            this.radioButtonSobelHx.Name = "radioButtonSobelHx";
            this.radioButtonSobelHx.Size = new System.Drawing.Size(68, 17);
            this.radioButtonSobelHx.TabIndex = 26;
            this.radioButtonSobelHx.Text = "Sobel Hx";
            this.radioButtonSobelHx.UseVisualStyleBackColor = true;
            this.radioButtonSobelHx.CheckedChanged += new System.EventHandler(this.radioButtonSobelHx_CheckedChanged);
            // 
            // radioButtonPrewittHy
            // 
            this.radioButtonPrewittHy.AutoSize = true;
            this.radioButtonPrewittHy.Location = new System.Drawing.Point(115, 132);
            this.radioButtonPrewittHy.Name = "radioButtonPrewittHy";
            this.radioButtonPrewittHy.Size = new System.Drawing.Size(73, 17);
            this.radioButtonPrewittHy.TabIndex = 25;
            this.radioButtonPrewittHy.Text = "Prewitt Hy";
            this.radioButtonPrewittHy.UseVisualStyleBackColor = true;
            this.radioButtonPrewittHy.CheckedChanged += new System.EventHandler(this.radioButtonPrewittHy_CheckedChanged);
            // 
            // radioButtonPrewittHx
            // 
            this.radioButtonPrewittHx.AutoSize = true;
            this.radioButtonPrewittHx.Location = new System.Drawing.Point(115, 109);
            this.radioButtonPrewittHx.Name = "radioButtonPrewittHx";
            this.radioButtonPrewittHx.Size = new System.Drawing.Size(73, 17);
            this.radioButtonPrewittHx.TabIndex = 24;
            this.radioButtonPrewittHx.Text = "Prewitt Hx";
            this.radioButtonPrewittHx.UseVisualStyleBackColor = true;
            this.radioButtonPrewittHx.CheckedChanged += new System.EventHandler(this.radioButtonPrewittHx_CheckedChanged);
            // 
            // radioButtonPassaAltas
            // 
            this.radioButtonPassaAltas.AutoSize = true;
            this.radioButtonPassaAltas.Location = new System.Drawing.Point(6, 155);
            this.radioButtonPassaAltas.Name = "radioButtonPassaAltas";
            this.radioButtonPassaAltas.Size = new System.Drawing.Size(79, 17);
            this.radioButtonPassaAltas.TabIndex = 23;
            this.radioButtonPassaAltas.Text = "Passa altas";
            this.radioButtonPassaAltas.UseVisualStyleBackColor = true;
            this.radioButtonPassaAltas.CheckedChanged += new System.EventHandler(this.radioButtonPassaAltas_CheckedChanged);
            // 
            // radioButtonLaplace
            // 
            this.radioButtonLaplace.AutoSize = true;
            this.radioButtonLaplace.Location = new System.Drawing.Point(7, 132);
            this.radioButtonLaplace.Name = "radioButtonLaplace";
            this.radioButtonLaplace.Size = new System.Drawing.Size(81, 17);
            this.radioButtonLaplace.TabIndex = 22;
            this.radioButtonLaplace.Text = "Laplaceano";
            this.radioButtonLaplace.UseVisualStyleBackColor = true;
            this.radioButtonLaplace.CheckedChanged += new System.EventHandler(this.radioButtonLaplace_CheckedChanged);
            // 
            // radioButtonGausse
            // 
            this.radioButtonGausse.AutoSize = true;
            this.radioButtonGausse.Location = new System.Drawing.Point(7, 109);
            this.radioButtonGausse.Name = "radioButtonGausse";
            this.radioButtonGausse.Size = new System.Drawing.Size(79, 17);
            this.radioButtonGausse.TabIndex = 21;
            this.radioButtonGausse.Text = "Gausseano";
            this.radioButtonGausse.UseVisualStyleBackColor = true;
            this.radioButtonGausse.CheckedChanged += new System.EventHandler(this.radioButtonGausse_CheckedChanged);
            // 
            // kernel33
            // 
            this.kernel33.DecimalPlaces = 4;
            this.kernel33.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel33.Location = new System.Drawing.Point(246, 83);
            this.kernel33.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel33.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel33.Name = "kernel33";
            this.kernel33.Size = new System.Drawing.Size(73, 20);
            this.kernel33.TabIndex = 21;
            // 
            // kernel23
            // 
            this.kernel23.DecimalPlaces = 4;
            this.kernel23.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel23.Location = new System.Drawing.Point(246, 57);
            this.kernel23.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel23.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel23.Name = "kernel23";
            this.kernel23.Size = new System.Drawing.Size(73, 20);
            this.kernel23.TabIndex = 20;
            // 
            // kernel13
            // 
            this.kernel13.DecimalPlaces = 4;
            this.kernel13.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel13.Location = new System.Drawing.Point(246, 31);
            this.kernel13.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel13.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel13.Name = "kernel13";
            this.kernel13.Size = new System.Drawing.Size(73, 20);
            this.kernel13.TabIndex = 19;
            // 
            // kernel32
            // 
            this.kernel32.DecimalPlaces = 4;
            this.kernel32.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel32.Location = new System.Drawing.Point(136, 83);
            this.kernel32.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel32.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel32.Name = "kernel32";
            this.kernel32.Size = new System.Drawing.Size(73, 20);
            this.kernel32.TabIndex = 18;
            // 
            // kernel22
            // 
            this.kernel22.DecimalPlaces = 4;
            this.kernel22.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel22.Location = new System.Drawing.Point(136, 57);
            this.kernel22.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel22.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel22.Name = "kernel22";
            this.kernel22.Size = new System.Drawing.Size(73, 20);
            this.kernel22.TabIndex = 17;
            this.kernel22.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // kernel12
            // 
            this.kernel12.DecimalPlaces = 4;
            this.kernel12.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel12.Location = new System.Drawing.Point(136, 31);
            this.kernel12.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel12.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel12.Name = "kernel12";
            this.kernel12.Size = new System.Drawing.Size(73, 20);
            this.kernel12.TabIndex = 16;
            // 
            // kernel31
            // 
            this.kernel31.DecimalPlaces = 4;
            this.kernel31.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel31.Location = new System.Drawing.Point(36, 83);
            this.kernel31.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel31.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel31.Name = "kernel31";
            this.kernel31.Size = new System.Drawing.Size(73, 20);
            this.kernel31.TabIndex = 15;
            // 
            // kernel21
            // 
            this.kernel21.DecimalPlaces = 4;
            this.kernel21.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel21.Location = new System.Drawing.Point(36, 57);
            this.kernel21.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel21.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel21.Name = "kernel21";
            this.kernel21.Size = new System.Drawing.Size(73, 20);
            this.kernel21.TabIndex = 14;
            // 
            // kernel11
            // 
            this.kernel11.DecimalPlaces = 4;
            this.kernel11.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.kernel11.Location = new System.Drawing.Point(36, 31);
            this.kernel11.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.kernel11.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.kernel11.Name = "kernel11";
            this.kernel11.Size = new System.Drawing.Size(73, 20);
            this.kernel11.TabIndex = 13;
            // 
            // groupBoxRotacionar
            // 
            this.groupBoxRotacionar.Controls.Add(this.buttonRotHor);
            this.groupBoxRotacionar.Controls.Add(this.buttonRotAntiHor);
            this.groupBoxRotacionar.Location = new System.Drawing.Point(199, 3);
            this.groupBoxRotacionar.Name = "groupBoxRotacionar";
            this.groupBoxRotacionar.Size = new System.Drawing.Size(169, 56);
            this.groupBoxRotacionar.TabIndex = 19;
            this.groupBoxRotacionar.TabStop = false;
            this.groupBoxRotacionar.Text = "Rotacionar";
            // 
            // buttonRotHor
            // 
            this.buttonRotHor.Location = new System.Drawing.Point(10, 19);
            this.buttonRotHor.Name = "buttonRotHor";
            this.buttonRotHor.Size = new System.Drawing.Size(73, 25);
            this.buttonRotHor.TabIndex = 0;
            this.buttonRotHor.Text = "Horário";
            this.buttonRotHor.UseVisualStyleBackColor = true;
            this.buttonRotHor.Click += new System.EventHandler(this.buttonRotHor_Click);
            // 
            // buttonRotAntiHor
            // 
            this.buttonRotAntiHor.Location = new System.Drawing.Point(89, 19);
            this.buttonRotAntiHor.Name = "buttonRotAntiHor";
            this.buttonRotAntiHor.Size = new System.Drawing.Size(73, 25);
            this.buttonRotAntiHor.TabIndex = 1;
            this.buttonRotAntiHor.Text = "Anti horário";
            this.buttonRotAntiHor.UseVisualStyleBackColor = true;
            this.buttonRotAntiHor.Click += new System.EventHandler(this.buttonRotAntiHor_Click);
            // 
            // labelEscalaHist
            // 
            this.labelEscalaHist.AutoSize = true;
            this.labelEscalaHist.Location = new System.Drawing.Point(13, 437);
            this.labelEscalaHist.Name = "labelEscalaHist";
            this.labelEscalaHist.Size = new System.Drawing.Size(152, 13);
            this.labelEscalaHist.TabIndex = 18;
            this.labelEscalaHist.Text = "Fator de escala do histograma:";
            // 
            // numericUpDownScaleHist
            // 
            this.numericUpDownScaleHist.DecimalPlaces = 3;
            this.numericUpDownScaleHist.Increment = new decimal(new int[] {
            2,
            0,
            0,
            196608});
            this.numericUpDownScaleHist.Location = new System.Drawing.Point(171, 435);
            this.numericUpDownScaleHist.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownScaleHist.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownScaleHist.Name = "numericUpDownScaleHist";
            this.numericUpDownScaleHist.Size = new System.Drawing.Size(73, 20);
            this.numericUpDownScaleHist.TabIndex = 17;
            this.numericUpDownScaleHist.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownScaleHist.ValueChanged += new System.EventHandler(this.numericUpDownScaleHist_ValueChanged);
            // 
            // boxHistogram
            // 
            this.boxHistogram.Controls.Add(this.glControl);
            this.boxHistogram.Location = new System.Drawing.Point(10, 453);
            this.boxHistogram.Name = "boxHistogram";
            this.boxHistogram.Size = new System.Drawing.Size(352, 242);
            this.boxHistogram.TabIndex = 15;
            this.boxHistogram.TabStop = false;
            this.boxHistogram.Text = "Histograma";
            // 
            // glControl
            // 
            this.glControl.BackColor = System.Drawing.Color.Black;
            this.glControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl.Location = new System.Drawing.Point(3, 16);
            this.glControl.Name = "glControl";
            this.glControl.Size = new System.Drawing.Size(346, 223);
            this.glControl.TabIndex = 0;
            this.glControl.VSync = false;
            this.glControl.Load += new System.EventHandler(this.glControl_Load);
            this.glControl.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl_Paint);
            // 
            // buttonNegativo
            // 
            this.buttonNegativo.Location = new System.Drawing.Point(170, 170);
            this.buttonNegativo.Name = "buttonNegativo";
            this.buttonNegativo.Size = new System.Drawing.Size(96, 25);
            this.buttonNegativo.TabIndex = 14;
            this.buttonNegativo.Text = "Negativo";
            this.buttonNegativo.UseVisualStyleBackColor = true;
            this.buttonNegativo.Click += new System.EventHandler(this.buttonNegativo_Click);
            // 
            // numericInContraste
            // 
            this.numericInContraste.DecimalPlaces = 2;
            this.numericInContraste.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.numericInContraste.Location = new System.Drawing.Point(193, 112);
            this.numericInContraste.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericInContraste.Name = "numericInContraste";
            this.numericInContraste.Size = new System.Drawing.Size(73, 20);
            this.numericInContraste.TabIndex = 13;
            this.numericInContraste.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericInContraste.ValueChanged += new System.EventHandler(this.numericInContraste_ValueChanged);
            // 
            // numericInBrilho
            // 
            this.numericInBrilho.Location = new System.Drawing.Point(193, 81);
            this.numericInBrilho.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericInBrilho.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.numericInBrilho.Name = "numericInBrilho";
            this.numericInBrilho.Size = new System.Drawing.Size(73, 20);
            this.numericInBrilho.TabIndex = 12;
            this.numericInBrilho.ValueChanged += new System.EventHandler(this.numericInBrilho_ValueChanged);
            // 
            // buttonContraste
            // 
            this.buttonContraste.Location = new System.Drawing.Point(272, 108);
            this.buttonContraste.Name = "buttonContraste";
            this.buttonContraste.Size = new System.Drawing.Size(96, 25);
            this.buttonContraste.TabIndex = 11;
            this.buttonContraste.Text = "Contraste";
            this.buttonContraste.UseVisualStyleBackColor = true;
            this.buttonContraste.Click += new System.EventHandler(this.buttonContraste_Click);
            // 
            // buttonBrilho
            // 
            this.buttonBrilho.Location = new System.Drawing.Point(272, 77);
            this.buttonBrilho.Name = "buttonBrilho";
            this.buttonBrilho.Size = new System.Drawing.Size(96, 25);
            this.buttonBrilho.TabIndex = 10;
            this.buttonBrilho.Text = "Brilho";
            this.buttonBrilho.UseVisualStyleBackColor = true;
            this.buttonBrilho.Click += new System.EventHandler(this.buttonBrilho_Click);
            // 
            // buttonEqualizar
            // 
            this.buttonEqualizar.Location = new System.Drawing.Point(266, 425);
            this.buttonEqualizar.Name = "buttonEqualizar";
            this.buttonEqualizar.Size = new System.Drawing.Size(96, 25);
            this.buttonEqualizar.TabIndex = 9;
            this.buttonEqualizar.Text = "Equalizar";
            this.buttonEqualizar.UseVisualStyleBackColor = true;
            this.buttonEqualizar.Click += new System.EventHandler(this.buttonEqualizar_Click);
            // 
            // labelTons
            // 
            this.labelTons.AutoSize = true;
            this.labelTons.Location = new System.Drawing.Point(153, 145);
            this.labelTons.Name = "labelTons";
            this.labelTons.Size = new System.Drawing.Size(34, 13);
            this.labelTons.TabIndex = 8;
            this.labelTons.Text = "Tons:";
            // 
            // buttonQuantizar
            // 
            this.buttonQuantizar.Location = new System.Drawing.Point(272, 139);
            this.buttonQuantizar.Name = "buttonQuantizar";
            this.buttonQuantizar.Size = new System.Drawing.Size(96, 25);
            this.buttonQuantizar.TabIndex = 7;
            this.buttonQuantizar.Text = "Quantizar";
            this.buttonQuantizar.UseVisualStyleBackColor = true;
            this.buttonQuantizar.Click += new System.EventHandler(this.buttonQuantizar_Click);
            // 
            // numericUpDownShades
            // 
            this.numericUpDownShades.Location = new System.Drawing.Point(193, 143);
            this.numericUpDownShades.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownShades.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownShades.Name = "numericUpDownShades";
            this.numericUpDownShades.Size = new System.Drawing.Size(73, 20);
            this.numericUpDownShades.TabIndex = 6;
            this.numericUpDownShades.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // groupBoxEspelhar
            // 
            this.groupBoxEspelhar.Controls.Add(this.buttonEspelhaX);
            this.groupBoxEspelhar.Controls.Add(this.buttonEspelhaY);
            this.groupBoxEspelhar.Location = new System.Drawing.Point(3, 3);
            this.groupBoxEspelhar.Name = "groupBoxEspelhar";
            this.groupBoxEspelhar.Size = new System.Drawing.Size(169, 56);
            this.groupBoxEspelhar.TabIndex = 5;
            this.groupBoxEspelhar.TabStop = false;
            this.groupBoxEspelhar.Text = "Espelhar";
            // 
            // buttonEspelhaX
            // 
            this.buttonEspelhaX.Location = new System.Drawing.Point(10, 19);
            this.buttonEspelhaX.Name = "buttonEspelhaX";
            this.buttonEspelhaX.Size = new System.Drawing.Size(73, 25);
            this.buttonEspelhaX.TabIndex = 0;
            this.buttonEspelhaX.Text = "Horizontal";
            this.buttonEspelhaX.UseVisualStyleBackColor = true;
            this.buttonEspelhaX.Click += new System.EventHandler(this.buttonEspelhaX_Click);
            // 
            // buttonEspelhaY
            // 
            this.buttonEspelhaY.Location = new System.Drawing.Point(89, 19);
            this.buttonEspelhaY.Name = "buttonEspelhaY";
            this.buttonEspelhaY.Size = new System.Drawing.Size(73, 25);
            this.buttonEspelhaY.TabIndex = 1;
            this.buttonEspelhaY.Text = "Vertical";
            this.buttonEspelhaY.UseVisualStyleBackColor = true;
            this.buttonEspelhaY.Click += new System.EventHandler(this.buttonEspelhaY_Click);
            // 
            // buttonEscalaCinza
            // 
            this.buttonEscalaCinza.Location = new System.Drawing.Point(272, 170);
            this.buttonEscalaCinza.Name = "buttonEscalaCinza";
            this.buttonEscalaCinza.Size = new System.Drawing.Size(96, 25);
            this.buttonEscalaCinza.TabIndex = 2;
            this.buttonEscalaCinza.Text = "Tons de cinza";
            this.buttonEscalaCinza.UseVisualStyleBackColor = true;
            this.buttonEscalaCinza.Click += new System.EventHandler(this.buttonEscalaCinza_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1394, 728);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1410, 766);
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Khin\'s FPI PhotoChoppe 0.2 - ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModificada)).EndInit();
            this.groupBoxZoom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomOutY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomOutX)).EndInit();
            this.groupBoxKernel.ResumeLayout(false);
            this.groupBoxKernel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kernel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kernel11)).EndInit();
            this.groupBoxRotacionar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleHist)).EndInit();
            this.boxHistogram.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericInContraste)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericInBrilho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShades)).EndInit();
            this.groupBoxEspelhar.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem arquivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator salvarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvarComoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fecharToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBoxOriginal;
        private System.Windows.Forms.PictureBox pictureBoxModificada;
        private System.Windows.Forms.ToolStripMenuItem salvarStripMenuItem;
        private System.Windows.Forms.Button buttonEspelhaX;
        private System.Windows.Forms.Button buttonEspelhaY;
        private System.Windows.Forms.Button buttonEscalaCinza;
        private System.Windows.Forms.GroupBox groupBoxEspelhar;
        private System.Windows.Forms.ToolStripMenuItem imagemToolStripMenuItem;
        private System.Windows.Forms.Button buttonQuantizar;
        private System.Windows.Forms.NumericUpDown numericUpDownShades;
        private System.Windows.Forms.ToolStripMenuItem espelharImagemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tonsDeCinzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quantizarToolStripMenuItem;
        private System.Windows.Forms.Label labelTons;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem desfazerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descartarMudançasToolStripMenuItem;
        private System.Windows.Forms.Button buttonContraste;
        private System.Windows.Forms.Button buttonBrilho;
        private System.Windows.Forms.Button buttonEqualizar;
        private System.Windows.Forms.Button buttonNegativo;
        private System.Windows.Forms.NumericUpDown numericInContraste;
        private System.Windows.Forms.NumericUpDown numericInBrilho;
        private System.Windows.Forms.GroupBox boxHistogram;
        private OpenTK.GLControl glControl;
        private System.Windows.Forms.Label labelEscalaHist;
        private System.Windows.Forms.NumericUpDown numericUpDownScaleHist;
        private System.Windows.Forms.GroupBox groupBoxKernel;
        private System.Windows.Forms.GroupBox groupBoxRotacionar;
        private System.Windows.Forms.Button buttonRotHor;
        private System.Windows.Forms.Button buttonRotAntiHor;
        private System.Windows.Forms.Button buttonConvolution;
        private System.Windows.Forms.RadioButton radioButtonNeutro;
        private System.Windows.Forms.RadioButton radioButtonSobelHy;
        private System.Windows.Forms.RadioButton radioButtonSobelHx;
        private System.Windows.Forms.RadioButton radioButtonPrewittHy;
        private System.Windows.Forms.RadioButton radioButtonPrewittHx;
        private System.Windows.Forms.RadioButton radioButtonPassaAltas;
        private System.Windows.Forms.RadioButton radioButtonLaplace;
        private System.Windows.Forms.RadioButton radioButtonGausse;
        private System.Windows.Forms.NumericUpDown kernel33;
        private System.Windows.Forms.NumericUpDown kernel23;
        private System.Windows.Forms.NumericUpDown kernel13;
        private System.Windows.Forms.NumericUpDown kernel32;
        private System.Windows.Forms.NumericUpDown kernel22;
        private System.Windows.Forms.NumericUpDown kernel12;
        private System.Windows.Forms.NumericUpDown kernel31;
        private System.Windows.Forms.NumericUpDown kernel21;
        private System.Windows.Forms.NumericUpDown kernel11;
        private System.Windows.Forms.GroupBox groupBoxZoom;
        private System.Windows.Forms.NumericUpDown numericUpDownZoomOutY;
        private System.Windows.Forms.NumericUpDown numericUpDownZoomOutX;
        private System.Windows.Forms.Button buttonZoomOut;
        private System.Windows.Forms.Button buttonZoomIn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}

