﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace trabFPI
{
    public partial class mainForm : Form
    {

        // ########## Constantes ##########
        private const string WindowTitle = "Khin's FPI PhotoChoppe 0.3 - ";


        // ########## Atributos ##########
        private bool isLoaded = false;    // Indica se existe alguma imagem carregada no momento atual.
        private string imgPath = "";    // diretório da imagem a carregar
        private Image null_image;            // Imagem nula de referência

        private Changes newImage;     // a imagem que sofrerá as modificações

        // ########## Métodos ##########
        public mainForm()
        {
            InitializeComponent();

            null_image = pictureBoxOriginal.Image;   // salva a imagem nula de referência
        }


        // ##### Funções do menu 'Arquivo'
        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msgExisteImg = "Já existe uma imagem carregada. Carregar uma nova imagem descartará a imagem atual, " +
                                "e todas as mudanças não salvas serão perdidas. Tem certeza que deseja continuar?";
            if (isLoaded)
                if (MessageBox.Show(msgExisteImg, "Descartar imagem atual", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;

            abrirNovaImagem();
        }

        private void salvarStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)    // se não existe imagem a ser gravada, retorna sem fazer nada
                return;
            if (imgPath == "")  // se por algum motivo essa condição não coincidir com !loaded (sempre deve coincidir)
                return;         // a condição está contemplada

            
            File.Delete(imgPath);

            newImage.getImagem().Save(imgPath, ImageFormat.Jpeg);
        }

        private void salvarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool arquivoExiste = true;  // a imagem não poderá ser salva até que se prove o contrário
            string msgArquivoExiste = "Um arquivo com este nome já existe. Deseja substituí-lo?";

            if (!isLoaded)    // se não existe imagem a ser gravada, retorna sem fazer nada
                return;

            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            do
            {
                if (saveFileDialog.ShowDialog() == DialogResult.Cancel) // se o usuário cancelar, sai do laço
                    break;

                arquivoExiste = File.Exists(saveFileDialog.FileName);

                if (arquivoExiste)
                    if (MessageBox.Show(msgArquivoExiste, "Sobrescrever arquivo",   // exibe mensagem de aviso
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        arquivoExiste = false;  // o arquivo existe, mas pode ser ignorado (porque o usuário permite)
            } while (arquivoExiste);    // mantém o laço enquanto o arquivo selecionado existir (real e logicamente) ou enquanto não cancelar

            if (!arquivoExiste)
            {
                imgPath = saveFileDialog.FileName;
                newImage.getImagem().Save(imgPath, ImageFormat.Jpeg);
                mainForm.ActiveForm.Text = WindowTitle + imgPath;
            }
        }
        
        private void fecharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msgFecharImg = "Tem certeza que deseja fechar as imagens? Quaisquer mudanças não salvas serão perdidas.";

            if (!isLoaded)
                return;

            if (MessageBox.Show(msgFecharImg, "Fechando arquivos", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return; // se o usuário cancelar, não faz nada.
            // else

            resetImg();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msgSaida = "Trabalhos não salvos serão perdidos. Tem certeza que deseja sair?";

            if (isLoaded)
            {
                if (MessageBox.Show(msgSaida, "Saindo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    resetImg();
                    Close();
                }
            }
            else
                Close();
        }


        // ##### Funções do menu 'Ferramentas'
        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.EspelharX();

            RefreshNovaImagem();
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.EspelharY();

            RefreshNovaImagem();
        }

        private void tonsDeCinzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.ToEscalaCinza();
            newImage.Operate(Changes.Operation.Cinza);

            RefreshNovaImagem();
        }

        private void quantizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.Quantizar((int)numericUpDownShades.Value);
            newImage.Operate(Changes.Operation.Quantizar, new float[]{ (float)numericUpDownShades.Value });

            RefreshNovaImagem();
        }

        private void desfazerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.Desfazer();

            RefreshNovaImagem();
        }

        private void descartarMudançasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            string msgDescarte = "Tem certeza que deseja descartar todas as alterações?";

            if (MessageBox.Show(msgDescarte, "Descartar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                newImage = new Changes(pictureBoxOriginal.Image);
                RefreshNovaImagem();
            }
        }


        // ##### Funções dos botões
        private void buttonEspelhaX_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.EspelharX();
            newImage.Operate(Changes.Operation.EspelharX);

            RefreshNovaImagem();
        }

        private void buttonEspelhaY_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.EspelharY();
            newImage.Operate(Changes.Operation.EspelharY);

            RefreshNovaImagem();
        }

        private void buttonEscalaCinza_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.ToEscalaCinza();
            newImage.Operate(Changes.Operation.Cinza);

            RefreshNovaImagem();
        }

        private void buttonQuantizar_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.Quantizar((int)numericUpDownShades.Value);
            newImage.Operate(Changes.Operation.Quantizar, new float[]{ (float)numericUpDownShades.Value} );

            RefreshNovaImagem();
        }

        private void buttonNegativo_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.Negativo();
            newImage.Operate(Changes.Operation.Negativo);

            RefreshNovaImagem();
        }

        private void buttonBrilho_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            float brilho = (float)numericInBrilho.Value;
            
            //newImage.Brilho(brilho);
            newImage.Operate(Changes.Operation.Brilho, new float[]{ brilho });

            RefreshNovaImagem();
        }

        private void numericInBrilho_ValueChanged(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            float brilho = (float)numericInBrilho.Value;

            //newImage.Brilho(brilho);
            newImage.Operate(Changes.Operation.Brilho, new float[]{ brilho });

            RefreshNovaImagem();
        }

        private void buttonContraste_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            float contraste = (float)numericInContraste.Value;

            //newImage.Contraste(contraste);
            newImage.Operate(Changes.Operation.Contraste, new float[]{ contraste });
            
            RefreshNovaImagem();
        }

        private void numericInContraste_ValueChanged(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            float contraste = (float)numericInContraste.Value;

            //newImage.Contraste(contraste);
            newImage.Operate(Changes.Operation.Contraste, new float[]{ contraste });

            RefreshNovaImagem();
        }

        private void buttonEqualizar_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            //newImage.Equalizar();
            newImage.Operate(Changes.Operation.Equalizar);

            RefreshNovaImagem();
        }


        // ##### Funções auxiliares
        private void abrirNovaImagem()
        {
            DialogResult resultadoOpenDialog;
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            resultadoOpenDialog = openFileDialog.ShowDialog();
            Stream buffer;

            if (resultadoOpenDialog == DialogResult.Cancel) // se o usuário cancela, não faz nada
                return;

            imgPath = openFileDialog.FileName;
            buffer = File.Open(imgPath, FileMode.Open);

            // Carrega e exibe a imagem nos dois 'pictureBox'
            pictureBoxOriginal.Image = Image.FromStream(buffer);
            buffer.Close();

            newImage = new Changes(pictureBoxOriginal.Image);

            pictureBoxModificada.Image = newImage.getImagem();

            // altera o valor do booleano loaded
            UpdateLoaded();
        }

        private void resetImg() // reseta as imagens para o padrão nulo, altera o flag 'loaded' e limpa a variável 'imgPath'
        {
            imgPath = "";

            pictureBoxOriginal.Image = null_image;
            pictureBoxModificada.Image = null_image;

            newImage = null;

            UpdateLoaded();
        }

        private void UpdateLoaded()
        {
            if (newImage == null)
            {
                isLoaded = false;
                ActiveForm.Text = WindowTitle;
            }
            else
            {
                isLoaded = true;
                ActiveForm.Text = WindowTitle + imgPath;
                glControl.Refresh();
            }
        }

        private void RefreshNovaImagem()
        {
            pictureBoxModificada.Image = newImage.getImagem();
            glControl.Refresh();
        }


        // ##### Funções de exibição do histograma
        private void glControl_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Ortho(0, 256, -10, 100, -1, 1);
        }

        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Ortho(0, 256, -10, 100, -1, 1);

            desenhaLegenda();

            if (isLoaded)
                desenhaHistograma();

            glControl.SwapBuffers();
        }

        private void desenhaLegenda()
        {
            GL.LineWidth(1.5f);
            GL.Begin(BeginMode.Lines);

            GL.Color3(Color.Blue);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(256, 0, 0);

            GL.End();


            GL.Begin(BeginMode.Quads);

            GL.Color3(Color.Black);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(0, -10, 0);

            GL.Color3(Color.White);
            GL.Vertex3(256, -10, 0);
            GL.Vertex3(256, 0, 0);
            
            GL.End();
        }

        private void desenhaHistograma()
        {
            float fatorEscala = (float)numericUpDownScaleHist.Value;
            int[] histograma = newImage.getHistograma();
            int maximo = histograma.Max();

            Matrix4 escala = Matrix4.Scale(1.0f, fatorEscala, 1.0f);

            GL.LineWidth(1.5f);
            GL.Color3(Color.DeepSkyBlue);
            
            GL.MatrixMode(MatrixMode.Modelview);
            GL.MultMatrix(ref escala);

            GL.Begin(BeginMode.Lines);
            
            for (int tom = 0; tom < histograma.Length; tom++)
            {
                GL.Vertex3(tom, 0, 0);
                GL.Vertex3(tom, histograma[tom], 0);
            }

            GL.End();
        }

        private void numericUpDownScaleHist_ValueChanged(object sender, EventArgs e)
        {
            glControl.Refresh();
        }

        // ##### Funções da versão 0.3 #####
        private void radioButtonGausse_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonGausse.Checked)
                return;

            kernel11.Value = 0.0625M; kernel12.Value = 0.125M; kernel13.Value = 0.0625M;
            kernel21.Value = 0.125M; kernel22.Value = 0.25M; kernel23.Value = 0.125M;
            kernel31.Value = 0.0625M; kernel32.Value = 0.125M; kernel33.Value = 0.0625M;
        }

        private void radioButtonLaplace_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonLaplace.Checked)
                return;

            kernel11.Value = 0M; kernel12.Value = -1M; kernel13.Value = 0M;
            kernel21.Value = -1M; kernel22.Value = 4M; kernel23.Value = -1M;
            kernel31.Value = 0M; kernel32.Value = -1M; kernel33.Value = 0M;
        }

        private void radioButtonPassaAltas_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonPassaAltas.Checked)
                return;

            kernel11.Value = -1M; kernel12.Value = -1M; kernel13.Value = -1M;
            kernel21.Value = -1M; kernel22.Value = 8M; kernel23.Value = -1M;
            kernel31.Value = -1M; kernel32.Value = -1M; kernel33.Value = -1M;
        }

        private void radioButtonPrewittHx_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonPrewittHx.Checked)
                return;

            kernel11.Value = -1M; kernel12.Value = 0M; kernel13.Value = 1M;
            kernel21.Value = -1M; kernel22.Value = 0M; kernel23.Value = 1M;
            kernel31.Value = -1M; kernel32.Value = 0M; kernel33.Value = 1M;
        }

        private void radioButtonPrewittHy_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonPrewittHy.Checked)
                return;

            kernel11.Value = -1M; kernel12.Value = -1M; kernel13.Value = -1M;
            kernel21.Value = 0M; kernel22.Value = 0M; kernel23.Value = 0M;
            kernel31.Value = 1M; kernel32.Value = 1M; kernel33.Value = 1M;
        }

        private void radioButtonSobelHx_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonSobelHx.Checked)
                return;

            kernel11.Value = -1M; kernel12.Value = 0M; kernel13.Value = 1M;
            kernel21.Value = -2M; kernel22.Value = 0M; kernel23.Value = 2M;
            kernel31.Value = -1M; kernel32.Value = 0M; kernel33.Value = 1M;
        }

        private void radioButtonSobelHy_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonSobelHy.Checked)
                return;

            kernel11.Value = -1M; kernel12.Value = -2M; kernel13.Value = -1M;
            kernel21.Value = 0M; kernel22.Value = 0M; kernel23.Value = 0M;
            kernel31.Value = 1M; kernel32.Value = 2M; kernel33.Value = 1M;
        }

        private void radioButtonNeutro_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButtonNeutro.Checked)
                return;

            kernel11.Value = 0M; kernel12.Value = 0M; kernel13.Value = 0M;
            kernel21.Value = 0M; kernel22.Value = 1M; kernel23.Value = 0M;
            kernel31.Value = 0M; kernel32.Value = 0M; kernel33.Value = 0M;
        }

        private void buttonRotAntiHor_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.Operate(Changes.Operation.RotateAntiClockwise);

            RefreshNovaImagem();
        }

        private void buttonRotHor_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.Operate(Changes.Operation.RotateClockwise);

            RefreshNovaImagem();
        }

        private void buttonZoomIn_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            newImage.Operate(Changes.Operation.ZoomIn);

            RefreshNovaImagem();
        }

        private void buttonZoomOut_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            float x, y;

            x = (float)numericUpDownZoomOutX.Value;
            y = (float)numericUpDownZoomOutY.Value;

            newImage.Operate(Changes.Operation.ZoomOut, new float[]{ x, y });

            RefreshNovaImagem();
        }

        private void buttonConvolution_Click(object sender, EventArgs e)
        {
            if (!isLoaded)
                return;

            DialogResult confirmaCinza = MessageBox.Show("Esta operação converterá a imagem para tons de cinza.\r\n" +
                        "Tem certeza que deseja continuar?", "Convolução", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (confirmaCinza == DialogResult.No)
                return;

            float[] kernel = new float[9] { (float)kernel33.Value, (float)kernel32.Value, (float)kernel31.Value,
                                            (float)kernel23.Value, (float)kernel22.Value, (float)kernel21.Value,
                                            (float)kernel13.Value, (float)kernel12.Value, (float)kernel11.Value };

            // Kernel já rotacionado!
            newImage.Operate(Changes.Operation.Convolution, kernel);

            RefreshNovaImagem();
        }

    }
}
