﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace trabFPI
{
    // Controla as alterações feitas sobre a imagem
    class Changes
    {
        // ########## Enum ##########
        public enum Operation
        {
            EspelharX, EspelharY,
            Quantizar,
            Negativo, Cinza,
            Brilho, Contraste,
            Equalizar,

            RotateClockwise, RotateAntiClockwise,
            ZoomIn, ZoomOut,
            Convolution
        }

        // ########## Constantes ##########
        const int maxTons = 256;

        // ########## Atributos ##########
        private Bitmap imagemNova;
        private Bitmap imagemAnterior;

        private int xMax;
        private int yMax;

        private int fatorBrilho;
        private float fatorContraste;

        private int[] histograma;
        private bool isCinza;

        private float[,] kernel;

        // ########## Métodos ##########
        public Changes(Image imagem)
        {
            this.imagemNova = new Bitmap(imagem);

            xMax = imagem.Width;
            yMax = imagem.Height;

            fatorBrilho = 0;
            fatorContraste = 1;

            VerificaCinza();

            if (!isCinza)
                GaranteCinza();

            CalculaHistograma();

            this.imagemNova = new Bitmap(imagem);
            this.imagemAnterior = this.imagemNova.Clone() as Bitmap;

            kernel = new float[3, 3];
        }

        public void Operate(Operation op)
        {
            Operate(op, new float[]{ 0 });
        }

        public void Operate(Operation op, float[] fator)
        {
            if (op == Operation.EspelharX)
                EspelharX();
            else if (op == Operation.EspelharY)
                EspelharY();
            else if (op == Operation.Equalizar)
                Equalizar();
            else if (op == Operation.RotateAntiClockwise)
                RotateAntiClockwise();
            else if (op == Operation.RotateClockwise)
                RotateClockwise();
            else if (op == Operation.ZoomIn)
                ZoomIn();
            else if (op == Operation.ZoomOut)
                ZoomOut((int)fator[0], (int)fator[1]);
            else if (op == Operation.Convolution)
                Convolve(fator);
            else
                HardOnes(op, fator[0]);

        }

        private void HardOnes(Operation op, float fator)
        {
            salvarImagemRetorno();

            BitmapData imageData = imagemNova.LockBits(new Rectangle(0, 0, imagemNova.Width, imagemNova.Height),
                                                        ImageLockMode.ReadWrite, imagemNova.PixelFormat);

            int BytesPerPixel = Bitmap.GetPixelFormatSize(imagemNova.PixelFormat) / 8;
            byte[] Pixels = new byte[imageData.Stride * imagemNova.Height]; //ByteCount
            IntPtr PtrFirstPixel = imageData.Scan0;

            Marshal.Copy(PtrFirstPixel, Pixels, 0, Pixels.Length);
            int HeightInPixels = imageData.Height;
            int WidthInBytes = imageData.Width * BytesPerPixel;

            if (op == Operation.Brilho)
                Brilho((int)fator);
            else if (op == Operation.Contraste)
                Contraste(fator);
            else if (op == Operation.Negativo)
                Negativo(HeightInPixels, imageData.Stride, WidthInBytes, BytesPerPixel, Pixels);
            else if (op == Operation.Cinza)
                ToEscalaCinza(HeightInPixels, imageData.Stride, WidthInBytes, BytesPerPixel, Pixels);
            else if (op == Operation.Quantizar)
                Quantizar(HeightInPixels, imageData.Stride, WidthInBytes, BytesPerPixel, Pixels, (int)fator);

            Marshal.Copy(Pixels, 0, PtrFirstPixel, Pixels.Length);
            imagemNova.UnlockBits(imageData);

            isCinza = true;
        }

        // ##### Getters #####
        public Image getImagem()
        {
            Bitmap imagemRetorno = this.imagemNova.Clone() as Bitmap;

            aplicaBrilho(imagemRetorno);
            aplicaContraste(imagemRetorno);

            CalculaHistograma(imagemRetorno);

            return imagemRetorno;
        }

        public int[] getHistograma()
        {
            return this.histograma;
        }


        // ###### Operações #####
        public void EspelharX() // espelha a imagem horizontalmente
        {
            int linhaInicial = 0;
            int linhaFinal = imagemNova.Height - 1;

            salvarImagemRetorno();

            while (linhaInicial < linhaFinal)
            {
                SwapLinhaPixel(linhaInicial, linhaFinal);

                linhaInicial++;
                linhaFinal--;
            }
        }

        public void EspelharY() // espelha a imagem verticalmente
        {
            int colunaInicial = 0;
            int colunaFinal = imagemNova.Width - 1;

            salvarImagemRetorno();

            while (colunaInicial < colunaFinal)
            {
                SwapColunaPixel(colunaInicial, colunaFinal);

                colunaInicial++;
                colunaFinal--;
            }
        }

        private void ToEscalaCinza(int HeightInPixels, int imageDataStride, int WidthInBytes, int BytesPerPixel, byte[] Pixels)
        {
            int CurrentLine;
            int OldBlue, OldGreen, OldRed, buffer;

            for (int y = 0; y < HeightInPixels; y++)
            {
                CurrentLine = y * imageDataStride;

                for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                {
                    OldBlue = Pixels[CurrentLine + x];
                    OldGreen = Pixels[CurrentLine + x + 1];
                    OldRed = Pixels[CurrentLine + x + 2];

                    buffer = (int)(0.299 * OldRed + 0.587 * OldGreen + 0.114 * OldBlue);

                    Pixels[CurrentLine + x] = (byte)(buffer);
                    Pixels[CurrentLine + x + 1] = (byte)(buffer);
                    Pixels[CurrentLine + x + 2] = (byte)(buffer);
                }
            }
        }

        private void Quantizar(int HeightInPixels, int imageDataStride, int WidthInBytes, int BytesPerPixel, byte[] Pixels, int tons)
        {
            int CurrentLine;
            int OldBlue, OldGreen, OldRed, buffer;

            for (int y = 0; y < HeightInPixels; y++)
            {
                CurrentLine = y * imageDataStride;

                for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                {
                    OldBlue = Pixels[CurrentLine + x];
                    OldGreen = Pixels[CurrentLine + x + 1];
                    OldRed = Pixels[CurrentLine + x + 2];


                    buffer = (int)((tons * (double)OldBlue) / maxTons);
                    buffer = (int)(buffer * maxTons / tons);
                    Pixels[CurrentLine + x] = (byte)(buffer);

                    buffer = (int)((tons * (double)OldGreen) / maxTons);
                    buffer = (int)(buffer * maxTons / tons);
                    Pixels[CurrentLine + x + 1] = (byte)(buffer);

                    buffer = (int)((tons * (double)OldRed) / maxTons);
                    buffer = (int)(buffer * maxTons / tons);
                    Pixels[CurrentLine + x + 2] = (byte)(buffer);
                }
            }
        }

        private void Negativo(int HeightInPixels, int imageDataStride, int WidthInBytes, int BytesPerPixel, byte[] Pixels)
        {
            int CurrentLine;
            int OldBlue, OldGreen, OldRed;

            for (int y = 0; y < HeightInPixels; y++)
            {
                CurrentLine = y * imageDataStride;

                for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                {
                    OldBlue = Pixels[CurrentLine + x];
                    OldGreen = Pixels[CurrentLine + x + 1];
                    OldRed = Pixels[CurrentLine + x + 2];

                    Pixels[CurrentLine + x] = (byte)(255 - OldBlue);
                    Pixels[CurrentLine + x + 1] = (byte)(255 - OldGreen);
                    Pixels[CurrentLine + x + 2] = (byte)(255 - OldRed);
                }
            }
        }

        public void Brilho(int brilho)
        {
            fatorBrilho = brilho;
        }

        public void Contraste(float contraste)
        {
            fatorContraste = contraste;
        }

        private void Equalizar()
        {
            salvarImagemRetorno();
            VerificaCinza();

            if (isCinza)
                EqualizarCinza();
            else
                EqualizarCores();
        }

        public void Desfazer()
        {
            if (imagemAnterior == null)
                return;
            imagemNova = (Bitmap)imagemAnterior.Clone();

            VerificaCinza();
            CalculaHistograma();
        }

        // Versão 0.3:
        private void RotateAntiClockwise()
        {
            Bitmap pixBuffer = new Bitmap(yMax, xMax); // vai trocar a orientação, é pra ser com os argumentos assim mesmo

            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                    pixBuffer.SetPixel(y, xMax - x - 1, imagemNova.GetPixel(x, y));

            int swap = xMax;
            xMax = yMax;
            yMax = swap;

            imagemNova = pixBuffer.Clone() as Bitmap;
        }

        private void RotateClockwise()
        {
            Bitmap pixBuffer = new Bitmap(yMax, xMax); // vai trocar a orientação, é pra ser com os argumentos assim mesmo

            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                    pixBuffer.SetPixel(yMax - y - 1, x, imagemNova.GetPixel(x, y));

            int swap = xMax;
            xMax = yMax;
            yMax = swap;

            imagemNova = pixBuffer.Clone() as Bitmap;
        }

        private void ZoomIn()
        {
            Bitmap pixBuffer;
            
            pixBuffer = MakeRoom();
            FillEmptyPixels(pixBuffer);

            xMax = pixBuffer.Width;
            yMax = pixBuffer.Height;

            imagemNova = pixBuffer.Clone() as Bitmap;
        }

        private void ZoomOut(int fatorX, int fatorY)
        {
            int[] media;
            int xMaxNovo = (int)Math.Ceiling((double)(xMax / fatorX));
            int yMaxNovo = (int)Math.Ceiling((double)(yMax / fatorY));
            Bitmap pixBuffer = new Bitmap(xMaxNovo, yMaxNovo);
            Color colorBuffer;

            for (int y = 0; y < yMaxNovo; y++)
                for (int x = 0; x < xMaxNovo; x++)
                {
                    media = new int[4] { 0, 0, 0, 0 };

                    for (int x2 = x * fatorX; x2 < x * fatorX + (fatorX - 1) && x2 < xMax; x2++)
                        for (int y2 = y * fatorY; y2 < y * fatorY + (fatorY - 1) && y2 < yMax; y2++)
                        {
                            colorBuffer = imagemNova.GetPixel(x2, y2);
                            media[0]++;
                            media[1] += colorBuffer.R;
                            media[2] += colorBuffer.G;
                            media[3] += colorBuffer.B;
                        }

                    media[1] /= media[0];
                    media[2] /= media[0];
                    media[3] /= media[0];

                    pixBuffer.SetPixel(x, y, Color.FromArgb(media[1], media[2], media[3]));
                }

            xMax = pixBuffer.Width;
            yMax = pixBuffer.Height;

            imagemNova = pixBuffer.Clone() as Bitmap;
        }

        private void Convolve(float[] kernel)
        {
            salvarImagemRetorno();

            GaranteCinza();

            Bitmap pixBuffer = new Bitmap(imagemNova);
            double colorBuffer;

            for (int y = 1; y < yMax - 1; y++)
                for (int x = 1; x < xMax - 1; x++)
                {
                    colorBuffer = imagemNova.GetPixel(x - 1, y - 1).G * kernel[0];
                    colorBuffer += imagemNova.GetPixel(x, y - 1).G * kernel[1];
                    colorBuffer += imagemNova.GetPixel(x + 1, y - 1).G * kernel[2];
                    colorBuffer += imagemNova.GetPixel(x - 1, y).G * kernel[3];
                    colorBuffer += imagemNova.GetPixel(x, y).G * kernel[4];
                    colorBuffer += imagemNova.GetPixel(x + 1, y).G * kernel[5];
                    colorBuffer += imagemNova.GetPixel(x - 1, y + 1).G * kernel[6];
                    colorBuffer += imagemNova.GetPixel(x, y + 1).G * kernel[7];
                    colorBuffer += imagemNova.GetPixel(x + 1, y + 1).G * kernel[8];

                    if (colorBuffer < 0)
                        colorBuffer = 0;

                    //colorBuffer += 125;

                    if (colorBuffer > 255)
                        colorBuffer = 255;

                    pixBuffer.SetPixel(x, y, Color.FromArgb((int)colorBuffer, (int)colorBuffer, (int)colorBuffer));
                }
            
            imagemNova = pixBuffer.Clone() as Bitmap;
        }

        // ##### Métodos auxiliares #####
        private void salvarImagemRetorno()
        {
            imagemAnterior = (Bitmap)imagemNova.Clone();
        }

        private void aplicaBrilho(Bitmap imagem)
        {
            BitmapData imageData = imagem.LockBits(new Rectangle(0, 0, imagem.Width, imagem.Height),
                                                        ImageLockMode.ReadWrite, imagem.PixelFormat);

            int BytesPerPixel = Bitmap.GetPixelFormatSize(imagem.PixelFormat) / 8;
            byte[] Pixels = new byte[imageData.Stride * imagemNova.Height]; //ByteCount
            IntPtr PtrFirstPixel = imageData.Scan0;

            Marshal.Copy(PtrFirstPixel, Pixels, 0, Pixels.Length);
            int HeightInPixels = imageData.Height;
            int WidthInBytes = imageData.Width * BytesPerPixel;

            int CurrentLine;
            int OldBlue, OldGreen, OldRed, buffer;

            for (int y = 0; y < HeightInPixels; y++)
            {
                CurrentLine = y * imageData.Stride;

                for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                {
                    OldBlue = Pixels[CurrentLine + x];
                    OldGreen = Pixels[CurrentLine + x + 1];
                    OldRed = Pixels[CurrentLine + x + 2];

                    buffer = OldBlue + fatorBrilho;
                    if (buffer > 255)
                        Pixels[CurrentLine + x] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x] = 0;
                    else
                        Pixels[CurrentLine + x] = (byte)(buffer);

                    buffer = OldGreen + fatorBrilho;
                    if (buffer > 255)
                        Pixels[CurrentLine + x + 1] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x + 1] = 0;
                    else
                        Pixels[CurrentLine + x + 1] = (byte)(buffer);

                    buffer = OldRed + fatorBrilho;
                    if (buffer > 255)
                        Pixels[CurrentLine + x + 2] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x + 2] = 0;
                    else
                        Pixels[CurrentLine + x + 2] = (byte)(buffer);
                }
            }

            Marshal.Copy(Pixels, 0, PtrFirstPixel, Pixels.Length);
            imagem.UnlockBits(imageData);
        }

        private void aplicaContraste(Bitmap imagem)
        {
            BitmapData imageData = imagem.LockBits(new Rectangle(0, 0, imagem.Width, imagem.Height),
                                                        ImageLockMode.ReadWrite, imagem.PixelFormat);

            int BytesPerPixel = Bitmap.GetPixelFormatSize(imagem.PixelFormat) / 8;
            byte[] Pixels = new byte[imageData.Stride * imagemNova.Height]; //ByteCount
            IntPtr PtrFirstPixel = imageData.Scan0;

            Marshal.Copy(PtrFirstPixel, Pixels, 0, Pixels.Length);
            int HeightInPixels = imageData.Height;
            int WidthInBytes = imageData.Width * BytesPerPixel;

            int CurrentLine;
            int OldBlue, OldGreen, OldRed, buffer;

            for (int y = 0; y < HeightInPixels; y++)
            {
                CurrentLine = y * imageData.Stride;

                for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                {
                    OldBlue = Pixels[CurrentLine + x];
                    OldGreen = Pixels[CurrentLine + x + 1];
                    OldRed = Pixels[CurrentLine + x + 2];

                    buffer = (int)(OldBlue * fatorContraste);
                    if (buffer > 255)
                        Pixels[CurrentLine + x] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x] = 0;
                    else
                        Pixels[CurrentLine + x] = (byte)(buffer);

                    buffer = (int)(OldGreen * fatorContraste);
                    if (buffer > 255)
                        Pixels[CurrentLine + x + 1] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x + 1] = 0;
                    else
                        Pixels[CurrentLine + x + 1] = (byte)(buffer);

                    buffer = (int)(OldRed * fatorContraste);
                    if (buffer > 255)
                        Pixels[CurrentLine + x + 2] = 255;
                    else if (buffer < 0)
                        Pixels[CurrentLine + x + 2] = 0;
                    else
                        Pixels[CurrentLine + x + 2] = (byte)(buffer);
                }
            }

            Marshal.Copy(Pixels, 0, PtrFirstPixel, Pixels.Length);
            imagem.UnlockBits(imageData);
        }

        private void SwapLinhaPixel(int a, int b)
        {
            Color bufferPixel;

            for (int x = 0; x < xMax; x++)
            {
                bufferPixel = imagemNova.GetPixel(x, a);
                imagemNova.SetPixel(x, a, imagemNova.GetPixel(x, b));
                imagemNova.SetPixel(x, b, bufferPixel);
            }
        }

        private void SwapColunaPixel(int a, int b)
        {
            Color bufferPixel;

            for (int y = 0; y < yMax; y++)
            {
                bufferPixel = imagemNova.GetPixel(a, y);
                imagemNova.SetPixel(a, y, imagemNova.GetPixel(b, y));
                imagemNova.SetPixel(b, y, bufferPixel);
            }
        }

        public void CalculaHistograma()
        {
            Bitmap imagem, keeper;

            keeper = imagemNova.Clone() as Bitmap;

            if (!isCinza)
                GaranteCinza();

            imagem = this.imagemNova.Clone() as Bitmap;

            aplicaBrilho(imagem);
            aplicaContraste(imagem);

            CalculaHistograma(imagem);

            imagemNova = keeper.Clone() as Bitmap;
        }

        public void CalculaHistograma(Bitmap imagem)
        {
            int tom = 0;
            histograma = new int[maxTons];

            for (int y = 0; y < yMax; y++)
                for (int x = 0; x < xMax; x++)
                {
                    tom = imagemNova.GetPixel(x, y).R;

                    histograma[tom]++;
                }
        }

        private void GaranteCinza()
        {
            VerificaCinza();
            if (isCinza)
                return;

            Operate(Operation.Cinza);
        }

        private void VerificaCinza()
        {
            //int x = 0, y = 0;
            Color pixelEmAnalise;
            isCinza = true;

            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++)
                {
                    pixelEmAnalise = imagemNova.GetPixel(x, y);

                    if (pixelEmAnalise.R != pixelEmAnalise.G)
                        isCinza = false;
                    if (pixelEmAnalise.R != pixelEmAnalise.B)
                        isCinza = false;
                    if (pixelEmAnalise.G != pixelEmAnalise.B)
                        isCinza = false;

                    if (!isCinza)
                        break;
                }
                
                if (!isCinza)
                    break;
            }
            /*
            while (isCinza && x < 256)
            {
                pixelEmAnalise = imagemNova.GetPixel(x, y);

                if (pixelEmAnalise.R != pixelEmAnalise.G)
                    isCinza = false;
                if (pixelEmAnalise.R != pixelEmAnalise.B)
                    isCinza = false;
                if (pixelEmAnalise.G != pixelEmAnalise.B)
                    isCinza = false;

                x++; y++;
            }*/
        }

        private void EqualizarCinza()
        {
            int[] histogramaCumulativo = new int[maxTons];
            double alfa = (double)(maxTons - 1) / (double)(xMax * yMax);
            int novoTomCinza;

            histogramaCumulativo[0] = (int)(alfa * histograma[0]);

            for (int i = 1; i < maxTons; i++)
                histogramaCumulativo[i] = (int)(histogramaCumulativo[i - 1] + alfa * histograma[i]);

            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                {
                    novoTomCinza = histogramaCumulativo[imagemNova.GetPixel(x, y).B];
                    imagemNova.SetPixel(x, y, Color.FromArgb(novoTomCinza, novoTomCinza, novoTomCinza));
                }

            CalculaHistograma();
        }

        private void EqualizarCores()
        {
            int[] histogramaCumulativo = new int[maxTons];
            double alfa = (double)(maxTons - 1) / (double)(xMax * yMax);
            int[] novoTom = new int[3];

            histogramaCumulativo[0] = (int)(alfa * histograma[0]);

            for (int i = 1; i < maxTons; i++)
                histogramaCumulativo[i] = (int)(histogramaCumulativo[i - 1] + alfa * histograma[i]);

            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                {
                    novoTom[0] = histogramaCumulativo[imagemNova.GetPixel(x, y).R];
                    novoTom[1] = histogramaCumulativo[imagemNova.GetPixel(x, y).G];
                    novoTom[2] = histogramaCumulativo[imagemNova.GetPixel(x, y).B];

                    imagemNova.SetPixel(x, y, Color.FromArgb(novoTom[0], novoTom[1], novoTom[2]));
                }

            CalculaHistograma();
        }

        private Bitmap MakeRoom()
        {
            int xMaxNovo = 2 * xMax -1;
            int yMaxNovo = 2 * yMax - 1;
            Bitmap target = new Bitmap(xMaxNovo, yMaxNovo);

            for (int y = 0, yNovo = 0; y < yMax; y++, yNovo += 2)
                for (int x = 0, xNovo = 0; x < xMax; x++, xNovo += 2)
                {
                    target.SetPixel(xNovo, yNovo, imagemNova.GetPixel(x, y));
                }

            return target;
        }

        private void FillEmptyPixels(Bitmap target)
        {
            int xMaxNovo = target.Width;
            int yMaxNovo = target.Height;
            Color media;

            for (int y = 0; y < yMaxNovo; y += 2)
                for (int x = 1; x < xMaxNovo; x += 2)
                {
                    media = Color.FromArgb((target.GetPixel(x - 1, y).R + target.GetPixel(x + 1, y).R) / 2,
                                           (target.GetPixel(x - 1, y).G + target.GetPixel(x + 1, y).G) / 2,
                                           (target.GetPixel(x - 1, y).B + target.GetPixel(x + 1, y).B) / 2);

                    target.SetPixel(x, y, media);
                }

            for (int y = 1; y < yMaxNovo; y += 2)
                for (int x = 0; x < xMaxNovo; x++)
                {
                    media = Color.FromArgb((target.GetPixel(x, y - 1).R + target.GetPixel(x, y + 1).R) / 2,
                                           (target.GetPixel(x, y - 1).G + target.GetPixel(x, y + 1).G) / 2,
                                           (target.GetPixel(x, y - 1).B + target.GetPixel(x, y + 1).B) / 2);

                    target.SetPixel(x, y, media);
                }
        }
    }
}
